package com.example.carikos;

/**
 *
 */

public class Public {
    public static final String API_KEY = "AIzaSyBjvNqYLw5DEpwqree8gJylsXofzR64ZY4";

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String CURTAB = "curtab";
    public static final String CURPAGE = "curpage";
    public static final String SERVER = "server";

    public static final String SERVER_URL = "http://carikos.yadiabdillah.com/carikos/api";
    public static final String BASE_URL = "http://carikos.yadiabdillah.com/";
    public static final String PARAMS_OAUTH_TOKEN = "OAuthToken";
    public static final String PARAMS_DEVICE_ID = "DeviceId";
    public static final String PARAMS_USERNAME = "Username";
    public static final String PARAMS_FIXUSERNAME = "FixUsername";
    public static final String PARAMS_GROUPNAME = "Groupname";
    public static final String PARAMS_USEREMAIL = "Useremail";
    public static final String PARAMS_PASSWORD = "Password";
    public static final String PARAMS_U_PASSWORD = "UPassTime";
    public static final String PARAMS_CASE = "AuthCase";

    public static final String REST_STATUS = "status";
    public static final String REST_MESSAGE = "message";
    public static final String REST_STATUS_SUCCESS = "success";

    public static final int SLEEP_SERVICE = 60000;

    public static final String SERVICE_SYNC_SIGN = "/master/login";
    public static final String SERVICE_SCHEDULE = "/master/schedule";


    public static final String FIRST_COLUMN="NO";
    public static final String SECOND_COLUMN="PRODUCT";
    public static final String THIRD_COLUMN="PRICE";
    public static final String FOURTH_COLUMN="QTY";
    public static final String FIFTH_COLUMN="TOTAL_PRICE";
}

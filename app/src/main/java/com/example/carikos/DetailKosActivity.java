package com.example.carikos;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import static com.example.carikos.Public.BASE_URL;

public class DetailKosActivity extends MyCore implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener,
        View.OnClickListener {

    CarouselView carouselView;
    TextView tvNama, tvAlamat, tvHarga, tvDeskripsi;
    android.support.v7.widget.GridLayout gridFitur;
    TextView tvGrid1, tvGrid2, tvGrid3, tvGrid4, tvGrid5, tvGrid6;
    ImageView imageGrid1, imageGrid2, imageGrid3, imageGrid4, imageGrid5, imageGrid6;
    LinearLayout boxGrid1, boxGrid2, boxGrid3, boxGrid4, boxGrid5, boxGrid6;
    Button btnBooking, btnDetail, btnLokasi;
    MapView mapView;
    LinearLayout mapLayout;

    int[] sampleImages = {R.drawable.kos1, R.drawable.kos2, R.drawable.kos3, R.drawable.kos4, R.drawable.kos5};
    int blankImage = R.drawable.img_blank_carikos;
    int notFoundImage = R.drawable.img_not_found;
    String[] sampleNetworkImageURLs = {
            "https://via.placeholder.com/350x150.png?text=image1",
            "https://via.placeholder.com/350x150.png?text=image2",
            "https://via.placeholder.com/350x150.png?text=image3",
            "https://via.placeholder.com/350x150.png?text=image4",
            "https://via.placeholder.com/350x150.png?text=image5"
    };

    String id_kos = "", nama_kos = "", id_vendor = "", alamat = "", harga = "", jumlah = "", deskripsi = "", ex_kasur = "", ex_lemari = "", ex_ac = "", ex_kipas = "", ex_wifi = "", ex_tv = "";
    String foto_kos = "", slider_kos = "";

    // google map variable
    private static final String TAG = "MapsActivity";
    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private GoogleApiClient googleApiClient;
    String myLat = "", myLong = "";
    int fristOpen = 0;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kos);

        // GET BUNDLE EXTRA DATA from previous activity
        getExtrasData();

        tvNama = (TextView) findViewById(R.id.tvNama);
        tvAlamat = (TextView) findViewById(R.id.tvAlamat);
        tvHarga = (TextView) findViewById(R.id.tvHarga);
        tvDeskripsi = (TextView) findViewById(R.id.tvDeskripsi);
        btnBooking = (Button) findViewById(R.id.btnBooking);
        btnBooking.setOnClickListener(this);
        btnDetail = (Button) findViewById(R.id.btnDetail);
        btnDetail.setOnClickListener(this);
        btnLokasi = (Button) findViewById(R.id.btnLokasi);
        btnLokasi.setOnClickListener(this);

        tvNama.setText(nama_kos);
        tvAlamat.setText(alamat);
        tvHarga.setText(harga);
        tvDeskripsi.setText(deskripsi);

        sampleNetworkImageURLs = explodeStringUsingStringUtils(slider_kos, ",");
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleNetworkImageURLs.length);
        carouselView.setImageListener(imageListener);

        gridFitur = (android.support.v7.widget.GridLayout) findViewById(R.id.gridFitur);
        tvGrid1 = (TextView) findViewById(R.id.tvGrid1);
        tvGrid2 = (TextView) findViewById(R.id.tvGrid2);
        tvGrid3 = (TextView) findViewById(R.id.tvGrid3);
        tvGrid4 = (TextView) findViewById(R.id.tvGrid4);
        tvGrid5 = (TextView) findViewById(R.id.tvGrid5);
        tvGrid6 = (TextView) findViewById(R.id.tvGrid6);

        imageGrid1 = (ImageView) findViewById(R.id.imageGrid1);
        imageGrid2 = (ImageView) findViewById(R.id.imageGrid2);
        imageGrid3 = (ImageView) findViewById(R.id.imageGrid3);
        imageGrid4 = (ImageView) findViewById(R.id.imageGrid4);
        imageGrid5 = (ImageView) findViewById(R.id.imageGrid5);
        imageGrid6 = (ImageView) findViewById(R.id.imageGrid6);

        boxGrid1 = (LinearLayout) findViewById(R.id.boxGrid1);
        boxGrid2 = (LinearLayout) findViewById(R.id.boxGrid2);
        boxGrid3 = (LinearLayout) findViewById(R.id.boxGrid3);
        boxGrid4 = (LinearLayout) findViewById(R.id.boxGrid4);
        boxGrid5 = (LinearLayout) findViewById(R.id.boxGrid5);
        boxGrid6 = (LinearLayout) findViewById(R.id.boxGrid6);

        boxGrid1.setVisibility(View.GONE);
        boxGrid2.setVisibility(View.GONE);
        boxGrid3.setVisibility(View.GONE);
        boxGrid4.setVisibility(View.GONE);
        boxGrid5.setVisibility(View.GONE);
        boxGrid6.setVisibility(View.GONE);

        String fitur[] = {ex_ac, ex_kasur, ex_kipas, ex_lemari, ex_tv, ex_wifi};
        String fiturName[] = {"AC", "Kasur", "Kipas", "Lemari", "TV", "Wifi"};
        int fiturIcon[] = {R.drawable.ac, R.drawable.bed, R.drawable.ic_kipas, R.drawable.lemari, R.drawable.tv, R.drawable.wifi};
        TextView fiturView[] = {tvGrid1, tvGrid2, tvGrid3, tvGrid4, tvGrid5, tvGrid6};
        ImageView fiturImage[] = {imageGrid1, imageGrid2, imageGrid3, imageGrid4, imageGrid5, imageGrid6};
        LinearLayout fiturBox[] = {boxGrid1, boxGrid2, boxGrid3, boxGrid4, boxGrid5, boxGrid6};

        Log.d("Fitur Value", ArrayUtils.toString(fitur));
        Log.d("fitur name", ArrayUtils.toString(fiturName));

        for (int i = 0; i < fitur.length; i++) {
            String selectedFitur = fitur[i];
            String selectedFiturName = fiturName[i];
            int selectedFiturIcon = fiturIcon[i];

            for (int j = 0; j < fitur.length; j++) {
                TextView selectedView = fiturView[j];
                ImageView selectedImageView = fiturImage[j];
                LinearLayout selectedBoxView = fiturBox[j];

                if (selectedView.getText().equals("null") && selectedFitur.equals("1")) {
                    selectedView.setText(selectedFiturName);
                    selectedImageView.setImageResource(selectedFiturIcon);
                    selectedBoxView.setVisibility(View.VISIBLE);
                    break;
                }
            }
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapLayout = (LinearLayout) findViewById(R.id.mapLayout);

        //Initializing googleApiClient
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    public void getExtrasData() {
        Bundle bundle = getIntent().getExtras();

        id_kos = bundle.getString("id_kos");
        nama_kos = bundle.getString("nama_kos");
        foto_kos = bundle.getString("foto_kos");
        slider_kos = bundle.getString("slider_kos");
        id_vendor = bundle.getString("id_vendor");
        alamat = bundle.getString("alamat");
        harga = bundle.getString("harga");
        jumlah = bundle.getString("jumlah");
        deskripsi = bundle.getString("deskripsi");

        ex_ac = bundle.getString("ex_ac");
        ex_kasur = bundle.getString("ex_kasur");
        ex_kipas = bundle.getString("ex_kipas");
        ex_lemari = bundle.getString("ex_lemari");
        ex_tv = bundle.getString("ex_tv");
        ex_wifi = bundle.getString("ex_wifi");

        latitude = Double.valueOf(bundle.getString("latitude"));
        longitude = Double.valueOf(bundle.getString("longitude"));
    }

    private String[] explodeStringUsingStringUtils(String stringToExplode, String separator) {
        return StringUtils.splitPreserveAllTokens(stringToExplode, separator);
    }

    // To set simple images
    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            String firstFoto = BASE_URL + sampleNetworkImageURLs[0].trim();
            String sliderFoto = BASE_URL + sampleNetworkImageURLs[position].trim();
            Log.d("slider_foto", sliderFoto);
            Picasso.get().load(sliderFoto).placeholder(blankImage).error(notFoundImage).fit().centerCrop().into(imageView);

            //imageView.setImageResource(sampleImages[position]);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnDetail:
                carouselView.setVisibility(View.VISIBLE);
                mapLayout.setVisibility(View.GONE);
                break;

            case R.id.btnLokasi:
                carouselView.setVisibility(View.GONE);
                mapLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.btnBooking:
                Intent intent = null;
                if (isLogin()) {
                    // checkout process
                    intent = new Intent(this, CheckoutActivity.class);
                    intent.putExtra("email", SP_EMAIL);
                } else {
                    // go to login page
                    setLoginRedirectActivity(ACTIVITY_CHECKOUT);
                    intent = new Intent(this, LoginActivity.class);
                }

                intent.putExtra("id_kos", id_kos);
                intent.putExtra("nama_kos", nama_kos);
                intent.putExtra("foto_kos", foto_kos);
                intent.putExtra("slider_kos", slider_kos);
                intent.putExtra("id_vendor", id_vendor);
                intent.putExtra("alamat", alamat);
                intent.putExtra("harga", harga);
                intent.putExtra("jumlah", jumlah);
                intent.putExtra("deskripsi", deskripsi);

                startActivity(intent);

                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        /*// close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }*/

        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
    }

    // Google Map Function

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // googleMapOptions.mapType(googleMap.MAP_TYPE_HYBRID)
        //    .compassEnabled(true);

        // Add a marker in Menara batavia and move the camera
        //latitude = -6.2107224;
        //longitude = 106.8156508;
        LatLng current = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(current).title("Marker in Current Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(current, 16));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            //moving the map to location
            moveMap();
        }
    }

    private void moveMap() {
        /**
         * Creating the latlng object to store lat, long coordinates
         * adding marker to map
         * move the camera with animation
         */
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Selected Place"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.getUiSettings().setZoomControlsEnabled(true);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        // mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Toast.makeText(DetailKosActivity.this, "onMarkerDragStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Toast.makeText(DetailKosActivity.this, "onMarkerDrag", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        // getting the Co-ordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //move to current position
        moveMap();
    }

    @Override
    protected void onStart() {
        //googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        //googleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}

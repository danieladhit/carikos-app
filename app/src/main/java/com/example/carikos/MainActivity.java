package com.example.carikos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;

import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.carikos.object.Kos;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends MyCore {
    List<Kos> listData;
    SwipeRefreshLayout swLayout;
    RecycleKos adapterList;
    ProgressDialog progressDialog;
    RecyclerView recycleList;
    int Home = 0;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Inisialisasi SwipeRefreshLayout
        swLayout = (SwipeRefreshLayout) findViewById(R.id.swlayout);

        // cek login
        if(!isLogin()) {
            setLoginRedirectActivity(ACTIVITY_HOME);
            LOGIN_REDIRECT_ACTIVITY = MainActivity.class;
            Log.d("is login ???", isLogin().toString());
            startActivity(new Intent(this,LoginActivity.class));
        }

        listData = new ArrayList<Kos>();
        adapterList = new RecycleKos(MainActivity.this, listData, false) {
            @Override
            public void setOnClickListener(Kos row) {
                //Toast.makeText(MainActivity.this, "Selected kos : " + row.getNama_kos(), Toast.LENGTH_SHORT).show();
                showDetailKos(row);
            }
        };

        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);

        // Mengeset listener yang akan dijalankan saat layar di refresh/swipe
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Handler untuk menjalankan jeda selama 5 detik
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        // Berhenti berputar/refreshing
                        swLayout.setRefreshing(false);

                        refreshData();
                    }
                }, 5000);
            }
        });

        recycleList = (RecyclerView) findViewById(R.id.recycleList);
        recycleList.setAdapter(adapterList);
        recycleList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recycleList.setItemAnimator(new DefaultItemAnimator());

        refreshData();
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }

        if(Home!=1){
            Home=1;
            Toast.makeText(this,"Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Home = 0;
                }
            }, 2000);
        } else {
            finish();
            moveTaskToBack(true);
            System.exit(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Hidden logout menu
        if(!isLogin()) {
            menu.getItem(2).setVisible(false); // logout menu
            menu.getItem(3).setVisible(false); // logout menu
        } else {
            menu.getItem(2).setVisible(true); // logout menu
            menu.getItem(3).setVisible(true); // logout menu
        }

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setQueryHint("Cari nama, alamat atau harga kos");

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapterList.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //listData.clear();
                // filter recycler view when text is changed
                adapterList.getFilter().filter(query);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            setLogout();
            //Toast.makeText(MainActivity.this, "Logout Succesfully !!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.orderHistory) {
            Intent intent = null;
            if (isLogin()) {
                intent = new Intent(this, HistoryActivity.class);
                intent.putExtra("email", SP_EMAIL);
            } else {
                setLoginRedirectActivity(ACTIVITY_HISTORY);
                intent = new Intent(this, LoginActivity.class);
            }
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void refreshData() {
        swLayout.setRefreshing(true);
        listData.clear();
        getServerData();
    }

    public void getServerData() {
        StringRequest sReq = new StringRequest(com.android.volley.Request.Method.POST,
                Public.SERVER_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // close dialog
                        Log.d("response", response);
                        try {
                            JSONArray responses = new JSONArray(response);
                            Log.d("response object", response.toString());
                            final int numberOfItemsInResp = responses.length();
                            if(numberOfItemsInResp > 0){
                                for(int i = 0; i < numberOfItemsInResp; i++) {
                                    JSONObject data = responses.getJSONObject(i);

                                    Kos row = new Kos();
                                    row.setId_kos(data.getString("id"));
                                    row.setNama_kos(data.getString("nama_kos"));
                                    row.setId_vendor(data.getString("id_vendor"));
                                    row.setAlamat(data.getString("alamat"));
                                    row.setHarga(data.getString("harga"));
                                    row.setJumlah(data.getString("jumlah"));
                                    row.setDeskripsi(data.getString("deskripsi"));
                                    row.setFoto_kos(data.getString("foto_kos"));
                                    row.setSlider_kos(data.getString("slider_kos"));
                                    row.setEx_ac(data.getString("ex_ac"));
                                    row.setEx_kasur(data.getString("ex_kasur"));
                                    row.setEx_kipas(data.getString("ex_kipas"));
                                    row.setEx_lemari(data.getString("ex_lemari"));
                                    row.setEx_tv(data.getString("ex_tv"));
                                    row.setEx_wifi(data.getString("ex_wifi"));

                                    row.setLatitude(data.getString("latitude"));
                                    row.setLongitude(data.getString("longitude"));

                                    listData.add(row);
                                }

                                adapterList.notifyDataSetChanged();

                            } else {
                                // get response data and convert to array
                                JSONObject data = responses.getJSONObject(0);

                                AlertDialog.Builder error = new AlertDialog.Builder(MainActivity.this);
                                error.setTitle("Connection Timeouttt");
                                error.setMessage(data.getString("message"));
                                error.create();
                                error.show();
                            }
                            swLayout.setRefreshing(false);
                        } catch (Exception e){
                            Log.d("error get schedule", e.toString());
                            swLayout.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String jsonError = "Connection timeout";
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            jsonError = new String(networkResponse.data);
                            // Print Error!
                        }
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        swLayout.setRefreshing(false);
                    }
                }) {
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("username", USERNAME);
                //params.put("day", Day);

                Log.d("params", params.toString());
                return params;
            }
        };
        Volley.newRequestQueue(MainActivity.this).add(sReq);
    }

    /** Called when the user taps the Send button */
    public void showDetailKos(Kos row) {
        Intent intent = new Intent(this, DetailKosActivity.class);

        intent.putExtra("id_kos", row.getId_kos());
        intent.putExtra("nama_kos", row.getNama_kos());
        intent.putExtra("foto_kos", row.getFoto_kos());
        intent.putExtra("slider_kos", row.getSlider_kos());
        intent.putExtra("id_vendor", row.getId_vendor());
        intent.putExtra("alamat", row.getAlamat());
        intent.putExtra("harga", "Rp. " + row.getHarga());
        intent.putExtra("jumlah", row.getJumlah());
        intent.putExtra("deskripsi", row.getDeskripsi());

        intent.putExtra("ex_ac", row.getEx_ac());
        intent.putExtra("ex_kasur", row.getEx_kasur());
        intent.putExtra("ex_kipas", row.getEx_kipas());
        intent.putExtra("ex_lemari", row.getEx_lemari());
        intent.putExtra("ex_tv", row.getEx_tv());
        intent.putExtra("ex_wifi", row.getEx_wifi());

        intent.putExtra("latitude", row.getLatitude());
        intent.putExtra("longitude", row.getLongitude());

        startActivity(intent);
    }

}
